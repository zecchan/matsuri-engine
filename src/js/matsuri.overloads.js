/**
 * Matsuri JS - Overloads
 */

// MatsuriBehavior

/** This will be called each update 
 * @param deltaT Delta time between this frame and last frame
*/
PIXI.Container.prototype.update = function (deltaT) { }

PIXI.Container.prototype._update = function (deltaT) {
    if (!this.enabled) return;
    this.update(deltaT);
    for(var i = 0; i < this.children.length; i++) {
        var c = this.children[i];
        if (c && typeof c._update === "function")
            c._update(deltaT);
    }
}

PIXI.Container.prototype.enabled = true;

PIXI.Sprite.prototype.setAnchor = function(x, y) {
    if (typeof x === "number") {
        this.anchor.x = x;
        if (typeof y === "number") {
            this.anchor.y = y;
        } else {
            this.anchor.y = x;
        }
    }
}