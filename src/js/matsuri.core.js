/**
 * Matsuri JS - Core
 */

class MatsuriEngine extends PIXI.Application {
    constructor(config) {
        super(config);

        this.version = "1.0";

        if (typeof config !== "object") config = {};
        config.container ||= "body";
        config.preload ||= false;

        this.Configuration = config;

        this.Scenes = new SceneManager();
        this.stage.addChild(this.Scenes);
        this.loader.onProgress.add((l, r) => {
            this.emmit('resourceloaded', [l, r]);
        });
    }

    _tick(d) {
        this.stage._update(d);
    }

    initialize() {
        console.log("Initializing MatsuriEngine v" + this.version + "...\r\n", this.Configuration);
        var cfg = this.Configuration;
        document.querySelector(cfg.container).appendChild(this.view);

        this.ticker.add(this._tick.bind(this));

        if (cfg.preload) {
            this.loader.add('preload.json', cfg.preload).load((l, r) => {
                if (r['preload.json'] && typeof r['preload.json'].data === "object") {
                    var lo = r['preload.json'].data;
                    console.log("Loading preload files...");
                    for(var k in lo) {
                        this.loader.add(k, lo[k]);
                    }
                    this.loader.load((l, r) => {
                        if (!l.loading)
                            this.initialized();
                    });
                } 
                else
                    this.initialized();
            });
        } else 
            this.initialized();
    }

    initialized() {
        this.emmit('initialized', []);
        console.log("Initialization finished!");
    }

    texture(id) {
        var res = this.loader.resources[id];
        if (res) 
            return res.texture || null;
        return null;
    }
    textures(a) {
        var ar = arguments;
        if (Array.isArray(a)) ar = a;
        var ret = [];
        for(var i = 0; i < ar.length; i++) {
            var id = ar[i];
            var res = this.loader.resources[id];
            if (res && res.texture) 
                ret.push(res.texture);
        }
        return ret;
    }
    isResourceLoaded(a) {
        var ar = arguments;
        if (Array.isArray(a)) ar = a;
        for(var i = 0; i < ar.length; i++) {
            var id = ar[i];
            var res = this.loader.resources[id];
            if (!res) 
                return false;
        }
        return true;
    }
}

function convertToEventEmitter(proto) {
    proto.on = function(event, callback) {
        this._eventPools[event] ||= [];
        this._eventPools[event].push(callback);
    };
    proto.emmit = function(event, args) {
        var pool = this._eventPools[event];
        if (pool) {
            for(var i = 0; i < pool.length; i++) {
                pool[i].apply(this, args);
            }
        }
    };
    proto._eventPools = [];
}

convertToEventEmitter(MatsuriEngine.prototype);

class SceneManager extends PIXI.Container {
    constructor() {
        super();
        this.current = null;
    }

    push(scene, paralel) {
        if (this.children.length > 0)
            this.children[this.children.length-1].enabled = paralel === true;
        this.addChild(scene);
        this.current = scene;
    }

    pop() {
        if (this.children.length <= 0) return;
        game.Scenes.removeChildAt(this.children.length-1);
        if (this.children.length > 0) {
            this.children[this.children.length-1].enabled = true;
            this.current = this.children[this.children.length-1];
        } else this.current = null;
    }
}

class Scene extends PIXI.Container { 
    constructor() {
        super();
        this.Layers = {};
    }

    setupLayers(names) {
        this.Layers = {};
        this.removeChildren();
        for(var i = 0; i < names.length; i++) {
            var l = new Layer();
            this.Layers[names[i]] = l;
            this.addChild(l);
        }
    }
}

class Layer extends PIXI.Container {    
    constructor() {
        super();
        this.sortableChildren = true;
    }
}